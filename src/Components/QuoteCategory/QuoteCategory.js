import React, {useEffect, useState} from 'react';
import axios from "axios";
import Quote from "../Quote/Quote";
import LeftMenu from "../LeftMenu/LeftMenu";

const QuoteCategory = ({match, history}) => {

    const [quotes, setQuotes] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://exam-8-42bf8-default-rtdb.firebaseio.com//quotes.json?orderBy="category"&equalTo="' + match.params.category + '"');
            setQuotes(response.data);
        };

        fetchData().catch(console.error);
    }, [match.params.category]);

    const quotesList = Object.entries(quotes).map(([key, value]) => {
        return (
            <Quote match={match} history={history} key={key} text={value.text} author={value.author} id={key}/>
        );
    })

    return (
        <div className="mainPage">
            <div className="leftMenu">
                <LeftMenu history={history}/>
            </div>
            <div className="quotes">
                {quotesList}
            </div>
        </div>
    );
};

export default QuoteCategory;