import React from 'react';
import './Quote.css'
import {NavLink} from "react-router-dom";
import axios from "axios";

const Quote = (props) => {

    const deleteQuote = () => {
        axios.delete('https://exam-8-42bf8-default-rtdb.firebaseio.com/quotes/' + props.id + '.json');
    }

    return (

        <div className="quote">
            <div className="quoteMessage">{props.text}</div>
            <div className="author"><i>{props.author}</i></div>
            <div className="quoteButtons">
                <NavLink className="Button" to={'quotes/' + props.id + '/edit'}>
                    Edit
                </NavLink>
                <button type="button" className="Button" onClick={deleteQuote}>Delete</button>
            </div>
        </div>
    );
};

export default Quote;