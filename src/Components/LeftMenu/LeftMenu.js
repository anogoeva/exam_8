import React from 'react';
import './LeftMenu.css';

export const categories = [
    {title: 'Star Wars', id: 'star-wars'},
    {title: 'Humor', id: 'humor'},
    {title: 'Motivational', id: 'motivational'},
    {title: 'Famous people', id: 'famous-people'},
    {title: 'Saying', id: 'saying'},
];

const LeftMenu = () => {

    return (
        <div>
            <ul>
                <li><a href="/">All</a></li>
                {categories.map(category => (
                    <li key={category.id}><a href={category.id}>{category.title}</a></li>
                ))}
            </ul>
        </div>
    );
};

export default LeftMenu;