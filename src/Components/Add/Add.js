import React, {useState} from 'react';
import axios from "axios";
import {categories} from "../LeftMenu/LeftMenu";
import './Add.css';

const Add = ({history}) => {

    const [quoteBody, setQuoteBody] = useState({
        category: '',
        text: '',
        author: ''
    });

    const addQuote = () => {
        const fetchData = async () => {
            axios({
                method: 'post',
                url: 'https://exam-8-42bf8-default-rtdb.firebaseio.com/quotes.json',
                headers: {},
                data: {
                    author: quoteBody.author,
                    category: quoteBody.category,
                    text: quoteBody.text
                }
            })
                .then(r => history.replace('/'));
        };
        fetchData().catch(e => console.error(e));
    };

    const onInputTextareaChange = e => {
        const {name, value} = e.target;
        setQuoteBody(prev => ({
            ...prev,
            [name]: value
        }));
    };

    return (
        <div className='form'>
            <h4>Submit new quote</h4>
            <form>
                <select name="category" id="category" onChange={onInputTextareaChange} className="form">
                    {categories.map(category => (
                        <option key={category.id} value={category.id} className="option-select">{category.title}</option>
                    ))}
                </select>
                <div className="author">
                    <p><label htmlFor="Title" className="authorP">Author</label></p>
                    <input
                        className="form"
                        name="author"
                        type="text"
                        value={quoteBody.author}
                        onChange={onInputTextareaChange}
                    />
                </div>
                <div>
                    <p><label htmlFor="Description" className="quoteP">Quote text</label></p>
                    <textarea
                        name="text"
                        value={quoteBody.text}
                        onChange={onInputTextareaChange} cols="55" rows="5"
                        className="form"/></div>
                <button onClick={addQuote} className="Button" type="button">Save</button>
            </form>
        </div>
    );
};

export default Add;