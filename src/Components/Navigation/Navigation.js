import React from 'react';
import {NavLink} from "react-router-dom";
import './Navigation.css';


const Navigation = () => {
    return (
        <div className="navigation">
            <ul>
                <li>
                    <NavLink
                        to='/'>Quotes</NavLink>
                </li>
                <li>
                    <NavLink
                        to='/add-quote'>Submit new quote</NavLink>
                </li>
            </ul>
        </div>
    );
};

export default Navigation;