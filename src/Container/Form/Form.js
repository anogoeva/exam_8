import React, {useEffect, useState} from 'react';
import axios from "axios";

const Form = ({match, history}) => {

    const [quoteBody, setQuoteBody] = useState({
        category: '',
        text: '',
        author: ''
    });

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://exam-8-42bf8-default-rtdb.firebaseio.com/quotes/' + match.params.id + '.json');
            setQuoteBody(response.data);
        };
        fetchData().catch(console.error);
    }, [match.params.id]);

    const editQuote = () => {
        const fetchData = async () => {
            axios({
                method: 'put',
                url: 'https://exam-8-42bf8-default-rtdb.firebaseio.com/quotes/' + match.params.id + '.json',
                headers: {},
                data: {
                    text: quoteBody.text,
                    author: quoteBody.author,
                    category: quoteBody.category
                }
            })
                .then(r => history.replace('/'));
        };
        fetchData().catch(e => console.error(e));
    };

    const onInputTextareaChange = e => {
        const {name, value} = e.target;
        setQuoteBody(prev => ({
            ...prev,
            [name]: value
        }));
    };

    return (
        <div className='form'>
            <h4>Edit a quote</h4>
            <form>
                <select name="categories" id="categories" className="form">
                    <option value={quoteBody.category}>{quoteBody.category}</option>
                </select>
                <div className="author">
                    <p><label htmlFor="Title" className="authorP">Author</label></p>
                    <input
                        className="form"
                        name="author"
                        type="text"
                        value={quoteBody.author}
                        onChange={onInputTextareaChange}
                    />
                </div>
                <div>
                    <p><label htmlFor="Description" className="quoteP">Quote text</label></p>
                    <textarea
                        name="text"
                        value={quoteBody.text}
                        onChange={onInputTextareaChange} cols="55" rows="5"
                        className="form"/></div>
                <button onClick={editQuote} className="Button" type="button">Save</button>
            </form>
        </div>
    );
};

export default Form;