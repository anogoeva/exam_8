import './App.css';
import {Route, Switch} from "react-router-dom";
import Navigation from "./Components/Navigation/Navigation";
import Quotes from "./Container/Quotes/Quotes";
import Form from "./Container/Form/Form";
import Add from "./Components/Add/Add";
import QuoteCategory from "./Components/QuoteCategory/QuoteCategory";

function App() {

    return (
        <div className="container block">
            <div className="header">
                <div className="quotesCentral"><p>Quotes Central</p></div>
                <div className="menu"><Navigation/></div>
            </div>
            <Switch>
                <Route path="/" exact component={Quotes}/>
                <Route path="/add-quote" component={Add}/>
                <Route path="/quotes/:id/edit" component={Form}/>
                <Route path="/:category" component={QuoteCategory}/>
            </Switch>

        </div>
    );
}

export default App;
